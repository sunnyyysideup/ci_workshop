# GitLab CI/CD Pipeline Example

## Project Overview

This repository demonstrates a basic CI/CD pipeline using GitLab CI/CD. The pipeline consists of several stages: environment setup, resource checks, testing, and packaging. The pipeline is configured using a `.gitlab-ci.yml` file and utilizes various Docker images to perform tasks in different environments.

## Files Description

### `.gitlab-ci.yml`

This file defines the GitLab CI/CD pipeline:

- **Stages**: The pipeline is divided into the following stages:
  - `env`: Prints the environment variables.
  - `check_res`: Checks system resources (HDD, RAM, CPU).
  - `test`: Runs a test to search for the phrase "Hello world" in `file1.txt` and `file2.txt`.
  - `package`: Creates a compressed package from `file1.txt` and `file2.txt`.

- **Jobs**:
  - **env**:
    - Uses the `alpine` image.
    - Runs the `env` command to print environment variables.
  - **job**:
    - Runs only on the `main` branch.
    - Concatenates `file1.txt` and `file2.txt`, searches for the phrase "Hello world", and stores the result in `cache_folder/cache.txt`.
  - **check_hdd**:
    - Uses the `debian` image.
    - Checks disk usage with `df -h`.
  - **check_ram**:
    - Uses the `ubuntu` image.
    - Checks memory usage with `free -mh`.
  - **check_cpu**:
    - Uses the `ubuntu:14.04` image.
    - Installs `git` and checks CPU information with `nproc` and `lscpu`.
  - **create package**:
    - Concatenates `file1.txt` and `file2.txt`, compresses the result into `package.gz`, and saves it as an artifact for 1 hour.

### `file1.txt`

- Contains the text: `Hello`.

### `file2.txt`

- Contains the text: `world`.

### `file3.txt`

- Contains the text: `my_new file`.

## Pipeline Execution

### Running the Pipeline

The pipeline will automatically run when changes are pushed to the `main` branch. The jobs in the pipeline will execute in the following order:

1. **env**: Prints environment variables.
2. **check_res**: 
   - `check_hdd`: Checks disk space.
   - `check_ram`: Checks memory usage.
   - `check_cpu`: Checks CPU information.
3. **test**: Searches for "Hello world" in the text files and caches the result.
4. **package**: Packages the concatenated text files into a compressed file (`package.gz`).

### Artifacts

- The `create package` job generates `package.gz`, which will be stored as an artifact and available for download for 1 hour after the job completes.

## Notes

- **Caching**: The `cache_folder` is used to store intermediate results between jobs, helping to speed up the pipeline execution.
- **Artifacts**: The final output (`package.gz`) is stored as an artifact that can be downloaded after the pipeline runs.
- **Resource Checks**: The pipeline includes jobs that check the status of system resources like disk usage, memory, and CPU, ensuring the environment is ready for subsequent stages.
